﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Self_service_Password_Reset.Models
{
    public class Self_service_Password_ResetContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public Self_service_Password_ResetContext() : base("name=Self_service_Password_ResetContext")
        {
        }

        public System.Data.Entity.DbSet<Self_service_Password_Reset.Hint> Hints { get; set; }

        public System.Data.Entity.DbSet<Self_service_Password_Reset.Question> Questions { get; set; }
    
    }
}
