﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Self_service_Password_Reset.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult DisplayMessage(string title = "", string messageTitle = "", string messageDescription = "", string redirectTo = "", int redirectDelay = 0, int messageType = 0)
        {
            ViewBag.Title = title;
            ViewBag.MessageTitle = messageTitle;
            ViewBag.MessageDescription = messageDescription;
            ViewBag.RedirectTo = redirectTo;
            ViewBag.RedirectDelay = redirectDelay;
            ViewBag.MessageType = messageType;

            //return View("/Views/Shared/DisplayMessage.cshtml");
			return View();
        }
    }
}
