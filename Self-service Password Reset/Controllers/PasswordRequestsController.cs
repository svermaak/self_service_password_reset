﻿using Self_service_Password_Reset.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Self_service_Password_Reset.Controllers
{
    public class PasswordRequestsController : Controller
    {
        private SSPREntities db = new SSPREntities();
        //private Self_service_Password_ResetContext db = new Self_service_Password_ResetContext();

        // GET: PasswordRequests
        public ActionResult Index()
        {
            return View();
        }

        // GET: PasswordRequests/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PasswordRequests/Create
        public ActionResult Create(string userName)
        {
            string message = "";

            if (userName == null)
            {
                message = "User Name not specified";
                return RedirectToAction("DisplayMessage", "Home", new { title = "Reset Password", messageTitle = "Error", messageDescription = @message, redirectTo = "../PasswordRequests/SpecifyUser", redirectDelay = 5, messageType = 3 });
            }

            //Hint hint = new Hint();
            List<Hint> hints = new List<Hint>();
            string encryptedUserName = Helpers.EncryptData(userName.Trim().ToUpper());

            hints = db.Hints.Where(u => u.UserName == encryptedUserName).ToList();

            if (hints.Count() < 4)
            {
                message = "User not configured for password self reset";
                return RedirectToAction("DisplayMessage", "Home", new { title = "Reset Password", messageTitle = "Warning", messageDescription = @message, redirectTo = "../PasswordRequests/SpecifyUser", redirectDelay = 5, messageType = 2 });
            }


            int[] questions;
            questions = Helpers.UniqueRandomNumber(3, 1, hints.Count());

            List<Hint> choosenHints = new List<Hint>();
            Hint hint = new Hint();

            foreach (int question in questions)
            {
                hint = hints[question - 1];
                //Clears answer so that they're not submitted to client and decrypts questions
                hint.Question = Helpers.DecryptData(hint.Question);
                hint.Answer = "";
                choosenHints.Add(hint);
            }

            return View(choosenHints);
        }

        // POST: PasswordRequests/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                string message = "";

                bool cancel = false;

                string userName = Helpers.DecryptData(collection["UserName"].Split(',')[0]);

                string password1 = collection["Password1"];
                string password2 = collection["Password2"];

                int question1 = 0;
                int question2 = 0;
                int question3 = 0;

                int.TryParse(collection["Question1"], out question1);
                int.TryParse(collection["Question2"], out question2);
                int.TryParse(collection["Question3"], out question3);

                string answer1 = collection["Answer1"].Trim().ToUpper();
                string answer2 = collection["Answer2"].Trim().ToUpper();
                string answer3 = collection["Answer3"].Trim().ToUpper();

                Hint hint1 = db.Hints.Where(h => h.ID == question1).FirstOrDefault();
                Hint hint2 = db.Hints.Where(h => h.ID == question2).FirstOrDefault();
                Hint hint3 = db.Hints.Where(h => h.ID == question3).FirstOrDefault();

                //Handles answers
                if ((cancel == false) && ((!Helpers.IsAnswerCorrect(hint1.Answer, answer1)) || (!Helpers.IsAnswerCorrect(hint2.Answer, answer2)) || (!Helpers.IsAnswerCorrect(hint3.Answer, answer3))))
                {
                    message = message + "Security questions answered incorrectly";
                    cancel = true;
                }

                //Handles mismatch passwords
                if ((cancel == false) && (password1 != password2))
                {
                    message = message + "Passwords do not match";
                    cancel = true;
                }

                //Handles insecure passwords
                if (cancel == false)
                {
                    bool valid = false;
                    string patternMatch = "";

                    Helpers.IsPasswordSecure(password1, out valid, out patternMatch);

                    if (valid == false)
                    {
                        message = message + "Password not secure, cannot contain " + patternMatch;
                        cancel = true;
                    }
                }

                if (cancel == true)
                {
                    return RedirectToAction("DisplayMessage", "Home", new { title = "Reset Password", messageTitle = "Warning", messageDescription = @message, redirectTo = "../PasswordRequests/SpecifyUser", redirectDelay = 5, messageType = 2 });
                }
                //If all checks passed, attemps password change
                else
                {
                    bool resetted = false;
                    try
                    {

                        string _domainFQDN = System.Configuration.ConfigurationManager.AppSettings["DomainFQDN"];
                        string _domainNetBIOS = System.Configuration.ConfigurationManager.AppSettings["DomainNetBIOS"];
                        string _connectionUserName = System.Configuration.ConfigurationManager.AppSettings["ConnectionUserName"];
                        string _connectionPassword = System.Configuration.ConfigurationManager.AppSettings["ConnectionPassword"];

                        using (DirectoryEntry de = new DirectoryEntry("LDAP://" + _domainFQDN, _domainNetBIOS + @"\" + _connectionUserName, _connectionPassword))
                        using (DirectorySearcher ds = new DirectorySearcher(de))
                        {

                            SearchResultCollection src;

                            ds.PageSize = 1000;
                            ds.PropertiesToLoad.Add("cn");
                            ds.PropertiesToLoad.Add("distinguishedName");
                            ds.PropertiesToLoad.Add("sAMAccountName");
                            ds.PropertiesToLoad.Add("userPrincipalName");
                            ds.PropertiesToLoad.Add("userAccountControl");
                            ds.PropertiesToLoad.Add("lockoutTime");
                            ds.Filter = "(&(userPrincipalName=" + userName + "))";
                            //ds.Filter = "(&(sAMAccountName=bsmith))";
                            ds.Sort.Direction = System.DirectoryServices.SortDirection.Ascending;
                            ds.Sort.PropertyName = "cn";
                            src = ds.FindAll();

                            foreach (SearchResult sr in src)
                            {
                                DirectoryEntry user = sr.GetDirectoryEntry();
                                user.Invoke("SetPassword", new object[] { password1 });

                                //********** Start of unlock user account
                                //try
                                //{
                                //    if (user.Properties.Contains("userAccountControl"))
                                //    {
                                //        int m_Val = (int)user.Properties["userAccountControl"][0];
                                //        de.Properties["userAccountControl"].Value = m_Val | 0x0001;
                                //        de.CommitChanges();
                                //    }
                                //}
                                //catch (Exception ex)
                                //{

                                //}
                                try
                                {
                                    user.Properties["LockOutTime"].Value = 0;
                                    user.CommitChanges();
                                }
                                catch (Exception ex)
                                {
                                    return RedirectToAction("DisplayMessage", "Home", new { title = "Reset Password", messageTitle = "Error", messageDescription = ex.Message, redirectTo = "../PasswordRequests/SpecifyUser", redirectDelay = 50, messageType = 3 });
                                }
                                //********** End of unlock user account

                                resetted = true;
                                break;
                            }
                        }

                        if (resetted == true)
                        {
                            return RedirectToAction("DisplayMessage", "Home", new { title = "Reset Password", messageTitle = "Success", messageDescription = "Password changed successfully", redirectTo = "../PasswordRequests/SpecifyUser", redirectDelay = 5, messageType = 1 });
                        }
                        else
                        {
                            return RedirectToAction("DisplayMessage", "Home", new { title = "Reset Password", messageTitle = "Not found", messageDescription = "User account not found", redirectTo = "../PasswordRequests/SpecifyUser", redirectDelay = 5, messageType = 2 });
                        }
                    }
                    catch (Exception ex)
                    {
                        return RedirectToAction("DisplayMessage", "Home", new { title = "Reset Password", messageTitle = "Error", messageDescription = ex.Message, redirectTo = "../PasswordRequests/SpecifyUser", redirectDelay = 5, messageType = 3 });
                    }

                }
            }
            catch
            {
                return View(ModelState);
            }
        }

        // GET: PasswordRequests/Create
        public ActionResult SpecifyUser()
        {
            return View();
        }

        // POST: PasswordRequests/SpecifyUser
        [HttpPost]
        public ActionResult SpecifyUser(string id)
        {
            try
            {
                if (id == null)
                {
                    return View();
                }

                return RedirectToAction("Create", "PasswordRequests", new { @userName = id });
            }
            catch
            {
                return View();
            }
        }

        // GET: PasswordRequests/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PasswordRequests/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PasswordRequests/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PasswordRequests/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
