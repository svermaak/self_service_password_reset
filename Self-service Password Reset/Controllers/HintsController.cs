﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Self_service_Password_Reset;
using Self_service_Password_Reset.Models;

namespace Self_service_Password_Reset.Controllers
{
    [Authorize]
    public class HintsController : Controller
    {
        private SSPREntities db = new SSPREntities();
        //private Self_service_Password_ResetContext db = new Self_service_Password_ResetContext();

        // GET: Hints
        public ActionResult Index()
        {
            //string userName = Helpers.EncryptData("TEST");
            string userName = Helpers.EncryptData(User.Identity.Name.Trim().ToUpper());

            List<Hint> hints = db.Hints.Where(h => h.UserName == userName).ToList();
            foreach (Hint hint in hints)
            {
                hint.Question = Helpers.DecryptData(hint.Question);
                hint.Answer = "";
            }
            return View(hints);
        }

        // GET: Hints/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hint hint = db.Hints.Find(id);
            hint.Question = Helpers.DecryptData(hint.Question);
            if (hint == null)
            {
                return HttpNotFound();
            }

            if (hint.UserName != Helpers.EncryptData(User.Identity.Name.Trim().ToUpper()))
            {
                string message = "Not allowed to create/view/edit/delete other users hints";
                return RedirectToAction("DisplayMessage", "Home", new { title = "Hints", messageTitle = "Warning", messageDescription = @message, redirectTo = "../Hints/Index", redirectDelay = 5, messageType = 2 });
            }

            return View(hint);
        }

        // GET: Hints/Create
        public ActionResult Create()
        {
            Hint hint = new Hint();
            //hint.UserName = Helpers.EncryptData("TEST");
            hint.UserName = Helpers.EncryptData(User.Identity.Name.Trim().ToUpper());
            return View(hint);
        }

        // POST: Hints/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,UserName,Question,Answer")] Hint hint)
        {
            if (ModelState.IsValid)
            {
                if (hint.UserName != Helpers.EncryptData(User.Identity.Name.Trim().ToUpper()))
                {
                    string message = "Not allowed to create/view/edit/delete other users hints";
                    return RedirectToAction("DisplayMessage", "Home", new { title = "Hints", messageTitle = "Warning", messageDescription = @message, redirectTo = "../Hints/Index", redirectDelay = 5, messageType = 2 });
                }

                hint.Question = Helpers.EncryptData(hint.Question);
                hint.Answer = Helpers.HashWithSalt(hint.Answer);

                db.Hints.Add(hint);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(hint);
        }

        // GET: Hints/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hint hint = db.Hints.Find(id);
            hint.Question = Helpers.DecryptData(hint.Question);
            hint.Answer = "";

            if (hint == null)
            {
                return HttpNotFound();
            }

            if (hint.UserName != Helpers.EncryptData(User.Identity.Name.Trim().ToUpper()))
            {
                string message = "Not allowed to create/view/edit/delete other users hints";
                return RedirectToAction("DisplayMessage", "Home", new { title = "Hints", messageTitle = "Warning", messageDescription = @message, redirectTo = "../Hints/Index", redirectDelay = 5, messageType = 2 });
            }

            return View(hint);
        }

        // POST: Hints/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,UserName,Question,Answer")] Hint hint)
        {
            if (ModelState.IsValid)
            {
                if (hint.UserName != Helpers.EncryptData(User.Identity.Name.Trim().ToUpper()))
                {
                    string message = "Not allowed to create/view/edit/delete other users hints";
                    return RedirectToAction("DisplayMessage", "Home", new { title = "Hints", messageTitle = "Warning", messageDescription = @message, redirectTo = "../Hints/Index", redirectDelay = 5, messageType = 2 });
                }

                hint.Question = Helpers.EncryptData(hint.Question);
                hint.Answer = Helpers.HashWithSalt(hint.Answer);

                db.Entry(hint).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hint);
        }

        // GET: Hints/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hint hint = db.Hints.Find(id);
            hint.Question = Helpers.DecryptData(hint.Question);
            if (hint == null)
            {
                return HttpNotFound();
            }

            if (hint.UserName != Helpers.EncryptData(User.Identity.Name.Trim().ToUpper()))
            {
                string message = "Not allowed to create/view/edit/delete other users hints";
                return RedirectToAction("DisplayMessage", "Home", new { title = "Hints", messageTitle = "Warning", messageDescription = @message, redirectTo = "../Hints/Index", redirectDelay = 5, messageType = 2 });
            }

            return View(hint);
        }

        // POST: Hints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hint hint = db.Hints.Find(id);

            if (hint.UserName != Helpers.EncryptData(User.Identity.Name.Trim().ToUpper()))
            {
                string message = "Not allowed to create/view/edit/delete other users hints";
                return RedirectToAction("DisplayMessage", "Home", new { title = "Hints", messageTitle = "Warning", messageDescription = @message, redirectTo = "../Hints/Index", redirectDelay = 5, messageType = 2 });
            }

            db.Hints.Remove(hint);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
