﻿/// <reference path="jquery-1.7.1.intellisense.js" />
/// <reference path="jquery-ui-1.10.2.js" />

$(document).ready(function () {
    //$("q").autocomplete({ source: $(this).attr("data-autocomplete") }
    $(":input[data-autocomplete]").each(function () {
        $(this).autocomplete({ source: $(this).attr("data-autocomplete") })
    });
    $(":input[date-datepicker]").datepicker({ dateFormat: "yy-mm-dd" });
    //$(":input[date-datepicker]").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");
})
