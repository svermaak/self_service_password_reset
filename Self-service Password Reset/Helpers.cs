﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;

namespace Self_service_Password_Reset
{
    public class Helpers
    {
        public static int[] UniqueRandomNumber(int numberOfRandomNumbers, int minimum, int maximum)
        {
            int i;
            int n = 3;
            int[] returnValues = new int[numberOfRandomNumbers];

            returnValues[0] = Helpers.GetRandomNumber(minimum, maximum);

            for (i = 1; i <= n - 1; i++)
            {
                bool notUnique = false;
                int randomNumber = 0;
                do
                {
                    notUnique = false;
                    randomNumber = Helpers.GetRandomNumber(minimum, maximum);
                    foreach (int returnValue in returnValues)
                    {
                        if (randomNumber == returnValue)
                        {
                            notUnique = true;
                        }
                    }
                } while (notUnique == true);
                returnValues[i] = randomNumber;
            }

            return returnValues;
        }

        public static int GetRandomNumber(int minimum, int maximum)
        {
            Random random = new Random();
            return random.Next(minimum, maximum + 1);
        }
        public static string EncryptData(string data)
        {
            Encryption64 encryption64 = new Encryption64();
            return encryption64.Encrypt(data, @"EwG50WMWxCZ2lu5h5JVB6dF022Q6Wabm12mLGLlIWeXmqY4Fd1vXY2mn6afVt5neTdTfuzG99q6zpxfaTZLSMC9LNkvSe4dvjAnu");
        }
        public static string DecryptData(string data)
        {
            Encryption64 encryption64 = new Encryption64();
            return encryption64.Decrypt(data, @"EwG50WMWxCZ2lu5h5JVB6dF022Q6Wabm12mLGLlIWeXmqY4Fd1vXY2mn6afVt5neTdTfuzG99q6zpxfaTZLSMC9LNkvSe4dvjAnu");
        }
        public static void IsPasswordSecure(string password, out Boolean valid, out string patternMatch)
        {
            bool cancel = false;
            bool bypassWordCheck = false;
            valid = false;
            patternMatch = "";

            //Evaluate length
            if ((cancel == false) && (password.Trim().Length <= 8))
            {
                patternMatch = "Should be longer than 8 charaters";
                cancel = true;
            }
            else if ((cancel == false) && (password.Trim().Length >= 16))
            {
                //If password 16 or longer don't hassle user about known words in password
                bypassWordCheck = true;
            }

            //Evaluate complexity

            //Evaluate weak passwords
            if ((cancel == false)&&(bypassWordCheck==false))
            {
                string weakPasswords = "password,gijima,123456,123456789,adobe123,12345678,qwerty,1234567,111111,photoshop,123123,1234567890,0,abc123,1234,adobe1,macromedia,azerty,iloveyou,aaaaaa,654321,12345,666666,sunshine,123321,letmein,monkey,asdfgh,password1,shadow,princess,dragon,adobeadobe,daniel,computer,michael,121212,charlie,master,superman,qwertyuiop,112233,asdfasdf,jessica,1q2w3e4r,welcome,1qaz2wsx,987654321,fdsa,753951,chocolate,fuckyou,soccer,tigger,asdasd,thomas,asdfghjkl,internet,michelle,football,123qwe,zxcvbnm,dreamweaver,7777777,maggie,qazwsx,baseball,jennifer,jordan,abcd1234,trustno1,buster,555555,liverpool,abc,whatever,11111111,102030,123123123,andrea,pepper,nicole,killer,abcdef,hannah,test,alexander,andrew,222222,joshua,freedom,samsung,asdfghj,purple,ginger,123654,matrix,secret,summer,1q2w3e,snoopy1,pa#sword,p#ssy,696969,mustang,pa#s,f#ckme,6969,harley,ranger,iwantu,hunter,f#ck,2000,batman,robert,access,love,hockey,george,sexy,a#shole,f#ckyou,dallas,panties,1111,austin,william,golfer,heather,hammer,yankees,biteme,enter,ashley,thunder,cowboy,silver,richard,f#cker,orange,merlin,corvette,bigdog,cheese,matthew,patrick,martin,bl#wjob,sparky,yellow,camaro,dick,falcon,taylor,131313,bitch,hello,scooter,please,porsche,guitar,chelsea,black,diamond,nascar,jackson,cameron,amanda,wizard,xxxxxxxx,money,phoenix,mickey,bailey,knight,iceman,tigers,horny,dakota,player,morgan,starwars,boomer,cowboys,edward,charles,girls,booboo,coffee,xxxxxx,bulldog,ncc1701,rabbit,peanut,john,johnny,gandalf,spanky,winter,brandy,compaq,carlos,tennis,james,mike,brandon,fender,anthony,blowme,ferrari,cookie,chicken,maverick,chicago,joseph,diablo,sexsex,hardcore,willie,chris,panther,yamaha,justin,banana,driver,marine,angels,fishing,david,maddog,hooters,wilson,butthead,dennis,f#cking,captain,bigdick,chester,smokey,xavier,steven,viking,snoopy,blue,eagles,winner,samantha,house,miller,flower,jack,firebird,butter,united,turtle,steelers,tiffany,zxcvbn,tomcat,golf,bond007,bear,tiger,doctor,gateway,gators,angel,junior,thx1138,porno,badboy,debbie,spider,melissa,booger,1212,flyers,fish,porn,teens,scooby,jason,walter,c#mshot,boston,braves,yankee,lover,barney,victor,tucker,mercedes,5150,doggie,zzzzzz,gunner,horney,bubba,2112,fred,johnson,xxxxx,tits,member,boobs,donald,bigdaddy,bronco,penis,voyager,rangers,birdie,trouble,white,topgun,bigtits,bitches,green,super,magic,lakers,rachel,slayer,scott,2222,asdf,video,london,7777,marlboro,srinivas,action,carter,jasper,monster,teresa,jeremy,11111111,bill,crystal,peter,p#ssies,c#ck,beer,rocket,theman,oliver,prince,beach,amateur,muffin,redsox,star,testing,shannon,murphy,frank,dave,eagle1,11111,mother,nathan,raiders,steve,forever,angela,viper,ou812,jake,lovers,suckit,gregory,buddy,young,nicholas,lucky,helpme,jackie,monica,midnight,college,baby,c#nt,brian,mark,startrek,sierra,leather,232323,4444,beavis,bigc#ck,happy,sophie,ladies,naughty,giants,booty,blonde,f#cked,golden,fire,sandra,pookie,packers,einstein,dolphins,chevy,winston,warrior,sammy,slut,8675309,nipples,power,victoria,vagina,toyota,travis,hotdog,paris,rock,xxxx,extreme,redskins,erotic,dirty,ford,freddy,arsenal,access14,wolf,nipple,alex,florida,eric,legend,movie,success,rosebud,jaguar,great,cool,cooper,1313,scorpio,mountain,madison,987654,brazil,lauren,japan,naked,squirt,stars,apple,alexis,aaaa,bonnie,peaches,jasmine,kevin,matt,qwertyui,danielle,beaver,4321,4128,runner,swimming,dolphin,gordon,casper,stupid,shit,saturn,gemini,apples,august,3333,canada,blazer,c#mming,hunting,kitty,rainbow,112233,arthur,cream,calvin,shaved,surfer,samson,kelly,paul,mine,king,racing,5555,eagle,hentai,newyork,little,redwings,smith,sticky,cocacola,animal,broncos,private,skippy,marvin,blondes,enjoy,girl,apollo,parker,qwert,time,sydney,women,voodoo,magnum,juice,abgrtyu,777777,dreams,maxwell,music,rush2112,russia,scorpion,rebecca,tester,mistress,phantom,billy,6666,albert";
                foreach (string weakPassword in weakPasswords.Split(','))
                {
                    if (password.Trim().ToUpper().IndexOf(weakPassword.Trim().ToUpper()) != -1)
                    {
                        patternMatch = weakPassword;
                        cancel = true;
                        break;
                    }
                }
            }

            if (cancel==false)
            {
                valid = true;
                patternMatch = "None";
            }
        }
        public static bool IsAnswerCorrect(string storedAnswer, string givenAnswer)
        {
            //Compare stored hash with given answer
            return storedAnswer.Split('|')[1] == Helpers.GetHashFromSalt(givenAnswer, storedAnswer.Split('|')[0]);
        }
        public static string HashWithSalt(string data)
        {
            string salt = CreateSalt(21);
            return salt + "|" + GetHashFromSalt(data.Trim().ToUpper(), salt);
        }
        private static string CreateSalt(int size)
        {
            //Generate a cryptographic random number.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);

            // Return a Base64 string representation of the random number.
            return Convert.ToBase64String(buff);
        }

        public static string GetHashFromSalt(string data, string salt)
        {
            string saltAndPwd = String.Concat(data, salt);
            string hashedPwd = GetSwcSH1(saltAndPwd);
            return hashedPwd;
        }
        private static string GetSwcSH1(string value)
        {
            SHA1 algorithm = SHA1.Create();
            byte[] data = algorithm.ComputeHash(Encoding.UTF8.GetBytes(value));
            string sh1 = "";
            for (int i = 0; i < data.Length; i++)
            {
                sh1 += data[i].ToString("x2").ToUpperInvariant();
            }
            return sh1;
        }

    }
}